package gestion;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class GestionTest {

	@Test
	void test() {
		Client monClient = new Client();
		monClient.creerClient("Marc Gilbert", 1, "Rue des tests");
		assertEquals("Rue des tests", monClient.getAdresse());
	}
	
	@Test
	void testNull() {
		Client monClient = new Client();
		monClient.creerClient("Marc Gilbert", 1, "");
		assertNull(monClient.getAdresse());
	}

}
