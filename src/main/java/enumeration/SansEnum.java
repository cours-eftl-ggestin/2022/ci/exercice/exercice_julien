package enumeration;

public class SansEnum {
	
	public static final int LUNDI = 1;
	public static final int MARDI = 2;
	
	public void methodeTest (int maDonnee) {
		if (maDonnee == LUNDI ) {
			//Traitement 1
		} else if (maDonnee == MARDI) {
			//Traitement 2
		}
	}

}
