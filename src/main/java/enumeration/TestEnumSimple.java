package enumeration;

enum Jours {
	LUNDI,
	MARDI,
	MERCREDI,
	JEUDI,
	VENDREDI,
	SAMEDI,
	DIMANCHE;
}

public class TestEnumSimple {
	
	public static void main(String[] args) {
		TestEnumSimple.test(Jours.MARDI);
		
		TestEnumSimple.test(Jours.VENDREDI);
	}
	
	public static void test(Jours quelJour) {
		switch(quelJour) {
			case LUNDI:
				System.out.println("Nous sommes lundi");
				break;
			case MARDI:
				System.out.println("Nous sommes mardi");
				break;	
			case MERCREDI:
				System.out.println("Nous sommes mercredi");
				break;
			case JEUDI:
				System.out.println("Nous sommes jeudi");
				break;
			case VENDREDI:
				System.out.println("Nous sommes vendredi");
				break;
			default :
				System.out.println("Nous sommes le weekend");
		}
	}
}
