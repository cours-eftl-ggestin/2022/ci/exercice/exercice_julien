package enumeration;

enum Jours2 {
	LUNDI(1, "Monday"),
	MARDI(2, "Tuesday"),
	MERCREDI(3, "Wednesday"),
	JEUDI(4, "Thursday"),
	VENDREDI(5, "Friday"),
	SAMEDI(6, "Saturday"),
	DIMANCHE(7, "Sunday");
	
	private int numeroJour;
	private String nomEnAnglais;
	
	private Jours2(int numeroJour, String nomEnAnglais) {
		this.numeroJour = numeroJour;
		this.nomEnAnglais = nomEnAnglais;
	}
	
	public int getNumeroJour() {
		return this.numeroJour;
	}
	
	public String getNomEnAnglais() {
		return this.nomEnAnglais;
	}

}

public class TestEnum {
	
	public static void main(String[] args) {
		
		TestEnum.test(Jours2.MARDI);
		
		TestEnum.test(Jours2.VENDREDI);
	}
	
	public static void test(Jours2 quelJour) {
		System.out.println("La valeur du jour = " + quelJour.getNumeroJour());
		System.out.println("Le nom en anglais du jour = " + quelJour.getNomEnAnglais());
		switch(quelJour) {
			case LUNDI:
				System.out.println("Nous sommes lundi");
				break;
			case MARDI:
				System.out.println("Nous sommes mardi, la valeur de cet élément est " 
								+ Jours2.MARDI.getNumeroJour());
				break;	
			case MERCREDI:
				System.out.println("Nous sommes mercredi");
				break;
			case JEUDI:
				System.out.println("Nous sommes jeudi");
				break;
			case VENDREDI:
				System.out.println("Nous sommes vendredi");
				break;
			default :
				System.out.println("Nous sommes le weekend");
		}
	}
}
