package exceptions;

public class Avion {
	
	private Boolean enVol;
	private Integer vitesseDecrochage;
	
	public Avion(Boolean enVol, Integer vitesseDecrochage) {
		this.enVol = enVol;
		this.vitesseDecrochage = vitesseDecrochage;
	}
	
	public String voler(Integer vitesse) throws ExceptionDecrochage {
		
		if (this.enVol && vitesse < this.vitesseDecrochage) {
			throw new ExceptionDecrochage(vitesse);
		}
		
		String message = this.enVol?"L'avion vole à " + vitesse:"L'avion ne vole pas";
		return message;
	}

}
