package exceptions;

public class Programme {

	public static void main(String[] args) {

		Tableaux tableau1 = new Tableaux();
		try {
			tableau1.afficheTableau();
			System.out.println("Autre instruction");
		} catch (Exception e) {
			System.out.println("Erreur lors de l'affichage du tableau : " + e.getMessage());
		} finally {
			System.out.println("Je suis dans le finally");
		}
		
		System.out.println("Fin normale du programme");

	}

}
