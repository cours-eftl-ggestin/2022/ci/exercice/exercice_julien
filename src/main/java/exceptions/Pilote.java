package exceptions;

public class Pilote {

	public static void main(String[] args) {
		Avion monAvion = new Avion(true, 200);
		String message = null;
		
		try {
			message = monAvion.voler(210);
		} catch (ExceptionDecrochage exception) {
			message = exception.getRaison();
		} finally {
			System.out.println(message);
		}

	}

}
