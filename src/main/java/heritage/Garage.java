package heritage;

public class Garage {
	
	public static String adresse = "Rue des Tests";

	public static void main(String[] args) {
		
		Vehicule vehicule1 = new Voiture(true, "Renault");
		vehicule1.rouler();
		
		Voiture voiture1 = new Voiture(true, "Peugeot");
		
		Vehicule vehicule2 = new Camion(false);
		vehicule2.rouler();

		vehicule1.demarrer();
		vehicule2.demarrer();
		
		Garage.trajet(vehicule2);
		
		Garage.trajet(vehicule1);
		
	}
	
	public static void trajet(Vehicule vehicule) {
		vehicule.demarrer();
		vehicule.rouler();
	}

}
