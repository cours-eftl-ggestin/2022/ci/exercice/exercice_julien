package heritage;

public class Voiture extends Vehicule {
	
	private String marque;
	
	public Voiture(Boolean type, String marque) {
		super(type);
		this.marque = marque;
	}
	
	@Override
	public void rouler() {
		System.out.println("La voiture de marque " + this.marque + " roule.");
	}

	public String getMarque() {
		return marque;
	}

	public void setMarque(String marque) {
		if (marque != null) {
			this.marque = marque;
		}
	}
	
	public String toString() {
		return "La voiture est de marque " + this.marque;
	}

}
