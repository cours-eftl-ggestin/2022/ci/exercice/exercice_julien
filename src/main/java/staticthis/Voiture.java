package staticthis;

public class Voiture {

	private String type;
	private static Integer nombreDeVoitures;
	
	public void creerVoiture(String type) {
		this.type = type;
		Voiture.nombreDeVoitures++;
	}
	
	public static Integer getNombreDeVoitures() {
		return Voiture.nombreDeVoitures;
	}
	
}
