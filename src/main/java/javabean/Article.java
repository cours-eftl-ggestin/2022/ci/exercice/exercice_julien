package javabean;

import java.io.Serializable;

public class Article implements Serializable {
	
	private Integer numero;
	private String libelle;
	
	public Article() {
		System.out.println("Exécution du constructeur");
	}

	public Integer getNumero() {
		return numero;
	}

	public void setNumero(Integer numero) {
		this.numero = numero;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

}
