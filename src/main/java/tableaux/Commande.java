package tableaux;

public class Commande {

	private Integer refCommande;
	private String nomClient;
	
	public Commande(Integer refCommande, String nomClient) {
		this.refCommande = refCommande;
		this.nomClient = nomClient;
	}
	
	public void affiche() {
		System.out.println("Commande N° " + this.refCommande + " pour le client " + this.nomClient);
	}
}
