package tableaux;

public class TableauMultiDimension {

	private String[][] tableau2d = { { "Oyonnax", "Gex", "Belley", "Bourg en Bresse" },
			{ "Laon", "Saint-Quentin", "Château-Thierry", "Hirson", "Guise" } };

	public void listeTableau() {
		int colonne = 0;
		int ligne = 0;
		for (String[] sousTab : tableau2d) {
			colonne = 0;
			for (String str : sousTab) {
				System.out.println("Valeur de l'élément : " + str);
				System.out.println("Valeur du tableau à l'indice [" + ligne + "][" + colonne + "] est : "
						+ tableau2d[ligne][colonne]);
				colonne++;
			}
			ligne ++;
		}
	}
}
