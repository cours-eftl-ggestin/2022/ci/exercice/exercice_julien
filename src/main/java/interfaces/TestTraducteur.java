package interfaces;

public class TestTraducteur {
	
	ITraducteur it ;
	
	public TestTraducteur(ITraducteur it) {
		this.it = it;
	}
	
	public String testTraduction() {
		return it.traduit("maison");
	}

}
