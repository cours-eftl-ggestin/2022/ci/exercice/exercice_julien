package interfaces;

public class Output {

	public Boolean write(String filename, String content) {
		System.out.println("file " + filename + " avec le contenu " + content + " a été écrit");
		return true;
	}
}
