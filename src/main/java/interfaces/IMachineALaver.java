package interfaces;

public interface IMachineALaver {
	
	enum Reglage {
		PRELAVAGE,
		LAVAGE,
		SECHAGE;
	}

	public void setReglage(Reglage reglage);
	
	public Reglage getReglage();
	
	public Boolean stopStart();
	
	/*
	 * Cette méthode permet de déclencher la fermeture du hublot
	 * de la machine à laver
	 * Cette méthode retourne true si la fermeture s'est effectuée
	 * correction, false sinon
	 */
	public Boolean fermerHublot();
	
	public Boolean ouvrirHublot();
}
