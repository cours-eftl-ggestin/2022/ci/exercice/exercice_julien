package interfaces;

public interface IRadio {

	public void setVolume(Integer volume);
	
	default void start() {
		System.out.println("Sonne comme une radio");
	}
}
