package interfaces;

public class Traducteur implements ITraducteur {

	public String traduit(String mot) {
		switch(mot) {
		case "maison":
			return "house";
		case "homme":
			return "man";
		default:
			return "unknown";
		}
	}
}
