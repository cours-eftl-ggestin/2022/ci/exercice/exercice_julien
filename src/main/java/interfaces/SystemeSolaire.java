package interfaces;

public class SystemeSolaire {
	public static void main(String[] args) {
		Thread t = new Thread(new Planete());
		t.start();
	}
}

class CorpsSpatial {

}

class Planete extends CorpsSpatial implements Runnable {
	public void run() {
		// Démarrage du Thread
		System.out.println("Thread!");
	}
}