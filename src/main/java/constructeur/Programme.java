package constructeur;

public class Programme {

	public static void main(String[] args) {
		Maison petite = new Maison("Brique", 60);
		
		Maison moyenne = new Maison("Pierre");
		
		Maison grande = new Maison("Pierre", 300, 200, 80);
	}
}
