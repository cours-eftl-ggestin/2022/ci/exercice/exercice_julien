package formationjava;

public class Syntaxe {

	public static void main(String[] args) {
		
		Double monDouble = 3.14159;
		String pi = Double.toString(monDouble);
		System.out.println("La valeur de pi = " + pi);
		
		String valeurNumerique = "100";
		Integer valeur = Integer.parseInt(valeurNumerique);
		System.out.println("La valeur est : " + valeur);
		
		int valeurPrimitiveUnboxing = valeur;
		System.out.println("La valeur unboxée en int est : " + valeurPrimitiveUnboxing);
		
		int valeurPrimitive = 2;
		Integer valeurObjetBoxee = valeurPrimitive;
		System.out.println("La valeur boxée en Integer est : " + valeurObjetBoxee);

		// Opérateurs
		Integer nombreArticles = 5;
		Double prixUnitaire = 12.45;
		Double total = nombreArticles * prixUnitaire;
		Double rabais = total / 10;
		Double aPayer = total - rabais;
		System.out.println("Total à payer = " + aPayer);
		
		Integer compteur = 10;
		compteur--;
		System.out.println("Valeur du compteur = " + compteur);
	
		int entier1 = 10;
		int entier2 = 10;
		System.out.println(entier1 >= entier2);
		
		String chaine1 = new String("Formation Java");
		String chaine2 = new String("Formation Java");
		System.out.println("Résultat comparaison référence = " + (chaine1 == chaine2));
		System.out.println("Résultat comparaison objet = " + chaine1.equals(chaine2));
	
		System.out.println(entier1==entier2 && chaine1.equals(chaine2));
		System.out.println(entier1==entier2 || chaine1.equals(chaine2));
		
		System.out.println(!(entier1==entier2) && chaine1.equals(chaine2));
		System.out.println(!(entier1==entier2 || chaine1.equals(chaine2)));
		
		// comparaison --> si c'est vrai : traitement 1 ; sinon : traitement 2
		// déterminer la valeur d'un int en fonction d'une comparaison
		// Si la comparaison retourne vrai : affectation de la valeur 10
		// Sinon : affectation de la valeur 20
		int resultat = (entier1==entier2)?10:20;
		System.out.println("Résultat = " + resultat);
		
		String maChaineEnFonctionDuTest = (chaine1.equals(chaine2))?"C'est égal":"Ce n'est pas égal";
		System.out.println("maChaineEnFonctionDuTest = " + maChaineEnFonctionDuTest);

		// If
		int temperature = -15;

		if (temperature < 0) {
			System.out.println("Il gèle !");
		} else {
			if (temperature == 0) {
				System.out.println("Point de transformation de l'eau en glace!");
			} else {
				System.out.println((temperature > 99)?"L'eau boue":"L'eau est liquide");
				/*if (temperature > 99) {
					System.out.println("L'eau boue");
				} else {
					System.out.println("L'eau est liquide");
				}*/
			}
		}
		
		// Switch
		char note = 'M';
		switch (note) {
			case 'A':
				System.out.println("Excellent");
				break;
			case 'B':
			case 'C':
				System.out.println("Bien");
				break;
			case 'D':
				System.out.println("Passable");
			case 'F':
				System.out.println("Recommencez!");
				break;
			default:
				System.out.println("Note inconnue");
		}
		System.out.println("Votre note est " + note);
		
		// Boucle for
		for (int compteurBoucle = 0; compteurBoucle < 5; compteurBoucle++) {
			System.out.println("Valeur courante de compteur = " + compteurBoucle);
		}
		
		String[] tableau = {"ain", "aisne", "allier"};
		System.out.println("Taille du tableau = " + tableau.length);
		for (int indice = 0; indice < tableau.length; indice++) {
			System.out.println(tableau[indice]);
		}
		
		// Boucle do while
		String[] villes = {"Paris", "Lyon", "Marseille"};
		Integer indice = 0;
		do {
			System.out.println(villes[indice]);
			indice++;
		} while (indice < villes.length);
		
		// Boucle while
		System.out.println("test while :");
		Integer increment = 0;
		while (increment < villes.length) {
			System.out.println(villes[increment]);
			increment++;
		}
		
		// Boucle while avec break/continue
		System.out.println("test break/continue :");
		
		Integer index = 0;
		String[] pays = {"France", "Italie", "Monaco", "Suisse", "Belgique", "USA", "Australie"};
		while (index < pays.length) {
			if (pays[index].contentEquals("Suisse")) {
				index++;
				continue;
			} 
			if (pays[index].contentEquals("USA")) {
				break;
			}
			System.out.println(pays[index]);
			index++;
		}
		
		// Boucle for intelligente
		System.out.println("test for intelligent :");
		for(String paysCourant : pays) {
			if (paysCourant.contentEquals("Suisse")) {
				continue;
			}
			if (paysCourant.contentEquals("USA")) {
				break;
			}
			System.out.println(paysCourant);
		}
	}
}
